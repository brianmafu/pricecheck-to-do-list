
var currently_selected_list_id = null;

 $('#id_create_list_btn').click(function(){

   		$.ajax({
        type: "POST",
        url: "/to-do-list/create-list/",
        data: {
        	'list_title': $('#id_list_title').val(),
        	'list_description': $('#id_list_description').val()
        }
        ,
        success: function(response) {
        	var response_data = JSON.parse(response);
   			$("#list-items-data-section").html('');
   			currently_selected_list_id = response_data['list_id'];
          	$('#id_display_message').text('List (' + response_data['title'] + ') has been successfully created');
           	$("#id_message").modal('show');
           	$(".selected-list-heading").html('List Name: ' + response_data['title']);
        },
        error: function() {
            $('#id_display_message').text('an error has occured while trying to created list. Please try again.');
           	$("#id_message").modal('show');
        }
    });
  });
$('#id_create_list_item_btn').click(function(){

   		$.ajax({
        type: "POST",
        url: "/to-do-list/create-to-do-list-item/" + currently_selected_list_id + '/',
        data: {
        	'list_item_title': $('#id_list_item_title').val(),
        	'list_item_description': $('#id_list_item_description').val()
        }
        ,
        success: function(response) {
        	var response_data = JSON.parse(response);
        	currently_selected_list_id  = response_data['list_id']
        		var complete = response_data['complete'] == true ? 'Yes' : 'No';
            	$("#list-items-data-section").append('<tr id="' + response_data['pk'] +  '" data-id="'+response_data['pk'] + '"><td>' + response_data['title'] + '</td><td>' + response_data['description'] + '</td><td>' +response_data['created'] + '</td><td class="complete_text" id="id_item_complete_'+ response_data['pk'] + '">' + complete +'</td><td><a href="#" class="delete-link" id="id_item_delete_' +  response_data['pk'] + '"> Check Off</a></br>Complete: <input class="complete_checkbox" type="checkbox" id="id_mark_item_complete_' + response_data['pk'] + '"/></input></td></tr>');
            	$("#id_item_delete_"+response_data['pk']).hide();
            	$('#id_item_delete_' + response_data['pk']).click(function(){
            		if (field_value['complete'] == false) {
            				$("#id_item_delete_"+response_data['pk']).hide();
            		}
        			$.ajax({
        			url: '/to-do-list/delete-to-do-list-item/' + response_data['pk'] + '/',
        			type: 'get',
        			success: function(data){
        				 $('#' + response_data['pk']).remove();
        				 $('#id_display_message').text('Item (' + response_data['title'] + ') has been removed');
       					 $("#id_message").modal('show');
        			},
        			error: function() {
            			$('#id_display_message').text('an error has occured while trying to delet list item. Please try again.');
           				$("#id_message").modal('show');
        			}

            		});
            	});
    
        		$('#id_mark_item_complete_' + response_data['pk']).click(function(){
            				$.ajax({
        					url: '/to-do-list/mark-to-do-list-item-complete/' + response_data['pk'] + '/',
        					type: 'get',
        					success: function(data){
        						var response_data = JSON.parse(data);
        						
        						var complete = response_data['complete'] == true ? 'Yes' : 'No';
        						var complete_alert_text = response_data['complete'] == true ? 'Complete' : 'Incomplete'
        						if(response_data['complete'] == true) {
        							$("#id_item_delete_"+response_data['pk']).show();
        						} else {
        							$("#id_item_delete_"+response_data['pk']).hide();
        						}
        						$("#id_item_complete_"+ response_data['pk']).text(complete);
        				 		$('#id_display_message').text('Item (' + response_data['title'] + ') has been marked as ' + complete_alert_text);
       					 		$("#id_message").modal('show');
        					},
        					error: function() {
            					$('#id_display_message').text('an error has occured while trying to updating list item. Please try again.');
           						$("#id_message").modal('show');
        				}

            				});
            			});

          	$('#id_display_message').text('List Item (' + response_data['title'] + ') has been successfully created');
           	$("#id_message").modal('show');

        },
        error: function() {
            $('#id_display_message').text('an error has occured while trying to created list. Please try again.');
           	$("#id_message").modal('show');
        }
    });
});

$("#id_mark_all_list_items_complete").click(function(){
		$.ajax({
        type: "get",
        url: "/to-do-list/mark-all-list-items-as-complete/" + currently_selected_list_id + '/',
        success: function(data) {
        	$(".complete_text").text("Yes");
        	$(".complete_checkbox").attr("checked", "checked");
            $(".delete-link").show();
       		$('#id_display_message').text('All Items Marked As Completed');
           	$("#id_message").modal('show');
        },
        error: function() {
            $('#id_display_message').text('an error has occured while trying to updating list item. Please try again.');
           	$("#id_message").modal('show');
        }

});
});

 $('#id_all_lists').click(function(){

   		$.ajax({
        type: "get",
        url: "/to-do-list/all-to-do-lists/",
        success: function(data) {
            var response_data = JSON.parse(data);
            var list_data = [];
            var count = 0;
            $.each(response_data, function(list_index, list_field_values){
            	count = count + 1;
            	list_data.push('<li class="list-group-item list-group-item-primary"><a href="#" id="id_link_list_item_'+ list_field_values['pk'] +'">' + count + '. ' + list_field_values['title'] + '</a></li>');
            });
            if (list_data.length > 0 ) {
            		$('#all-list-groups').html(list_data.join(''));
            } else {
            	$('#all-list-groups').html('<li class="list-group-item list-group-item-primary">currently no lists available</list>');
            }
            // Register list item onclick callbacks
            $.each(response_data, function(list_index, list_field_values){
            	  $('#id_link_list_item_'+ list_field_values['pk']).click(function(){
            	  	currently_selected_list_id = list_field_values['pk']
            		$.ajax({
            			type: 'get',
            			url: '/to-do-list/get-to-do-list/' + currently_selected_list_id + '/',
            			success: function(data) {
            				var item_rows = []
            				var to_data_list_items_data = JSON.parse(data)[0]['list_items'];
            				$(".selected-list-heading").html('List Name: ' + list_field_values['title']);
            				$.each(to_data_list_items_data, function(field_key, field_value){
            				var complete = field_value['complete'] == true ? 'Yes' : 'No';
            				item_rows.push('<tr id="' + field_value['pk'] +  '" data-id="'+field_value['pk'] + '"><td>' + field_value['title'] + '</td><td>' + field_value['description'] + '</td><td>' +field_value['created'] + '</td><td class="complete_text" id="id_item_complete_'+ field_value['pk'] + '">'+ complete +'</td><td><a class="delete-link" href="#" id="id_item_delete_' +  field_value['pk'] + '"> Check Off</a><br>Complete: <input type="checkbox" class="complete_checkbox" id="id_mark_item_complete_' + field_value['pk'] + '"/></td></tr>');

           
            			});
            			$("#list-items-data-section").html(item_rows.join(''));
            			$.each(to_data_list_items_data, function(field_key, field_value){ 
            			if (field_value['complete'] == false) {
            				$("#id_item_delete_"+field_value['pk']).hide();

            			} else {
            				$("#id_mark_item_complete_" + field_value['pk']).attr("checked", "checked")
            			}
            			$('#id_item_delete_' + field_value['pk']).click(function(){
            			
        					$.ajax({
        					url: '/to-do-list/delete-to-do-list-item/' + field_value['pk'] + '/',
        					type: 'get',
        					success: function(data){
        				 		$('#' + field_value['pk']).remove();
        				 		$('#id_display_message').text('Item (' + field_value['title'] + ') has been removed');
       					 		$("#id_message").modal('show');
        					},
        					error: function() {
            					$('#id_display_message').text('an error has occured while trying to delet list item. Please try again.');
           						$("#id_message").modal('show');
        				}

            				});
        				});
        				$('#id_mark_item_complete_' + field_value['pk']).click(function(){
   
            				$.ajax({
        					url: '/to-do-list/mark-to-do-list-item-complete/' + field_value['pk'] + '/',
        					type: 'get',
        					success: function(data){
        						var response_data = JSON.parse(data);
        						
        						var complete = response_data['complete'] == true ? 'Yes' : 'No';
        						if(response_data['complete'] == true) {
        								$("#id_item_delete_"+field_value['pk']).show();
        						} else {
        							$("#id_item_delete_"+field_value['pk']).hide();
        						}
        						var complete_alert_text = response_data['complete'] == true ? 'Complete' : 'Incomplete'
        						$("#id_item_complete_"+ response_data['pk']).text(complete);
        				 		$('#id_display_message').text('Item (' + field_value['title'] + ') has been marked as ' + complete_alert_text);
       					 		$("#id_message").modal('show');
        					},
        					error: function() {
            					$('#id_display_message').text('an error has occured while trying to updating list item. Please try again.');
           						$("#id_message").modal('show');
        				}

            				});
            			});
            			});

            			}
            		});
            	});
            });
        },
        error: function() {
            $('#id_display_message').text('could not retrieve all to do lists.');
           	$("#id_message").modal('show');
        }
    });
  });
