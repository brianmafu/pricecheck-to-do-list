from django.db import models
from datetime import datetime
# Create your models here.


class TodoListItem(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    created = models.DateField(default=datetime.now().strftime("%Y-%m-%d"))
    complete =  models.BooleanField(default=False)
    to_do_list_id = models.IntegerField(null=True, blank=True)
    class Meta:
        ordering = ["-created"]
    def __str__(self):
        return self.title

class TodoList(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    created = models.DateField(default=datetime.now().strftime("%Y-%m-%d"))
    due_date = models.DateField(default=datetime.now().strftime("%Y-%m-%d"))
    class Meta:
        ordering = ["-created"]
    def __str__(self):
        return self.title
