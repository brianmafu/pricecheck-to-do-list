from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from django.http import HttpResponseRedirect
from django.conf import settings
import json
from .models import *

 
 # To List Views

@csrf_exempt
def delete_to_do_list_item(request, pk=None):
    status = 'Success'
    try:
            TodoListItem.objects.filter(pk=pk, complete=True).delete()
    except:
        status = 'Failed'
    return HttpResponse(json.dumps({'status': status}))

@csrf_exempt
def mark_to_do_list_item_complete(request, pk=None):
    complete = True
    try:
        list_item = TodoListItem.objects.get(pk=pk)
        list_item.complete = not list_item.complete
        complete = list_item.complete
        list_item.save()
    except:
        complete = False
        pass

    return HttpResponse(json.dumps({'complete': complete, 'pk': pk}))

@csrf_exempt
def mark_all_list_items_as_complete(request, pk=None):
    complete = True
    try:
        TodoListItem.objects.filter(to_do_list_id=pk).update(complete=True)
    except:
        complete = False
        pass

    return HttpResponse(json.dumps({'complete': complete, 'pk': pk}))

@csrf_exempt
def create_to_do_list(request):
    if request.POST:
        list_title = request.POST['list_title']
        list_description = request.POST['list_description']
        # create list object
        new_list = TodoList.objects.create(**{
                'title': list_title,
                'description': list_description
        })
        new_list.save()
        return HttpResponse(json.dumps({'title': new_list.title, 'list_id': new_list.pk}))
    return HttpResponse(None)


@csrf_exempt
def create_to_do_list_item(request, pk=None):
    if request.POST:
        list_item_title = request.POST['list_item_title']
        list_item_description = request.POST['list_item_description']
        # create list object
        new_list_item = TodoListItem.objects.create(**{
                'title': list_item_title,
                'description': list_item_description
        })
        new_list_item.to_do_list_id = pk
        new_list_item.save()
        number_of_items = TodoListItem.objects.filter(to_do_list_id=pk).count();
        return HttpResponse(json.dumps({
            'pk': new_list_item.id, 'title': new_list_item.title,
            'list_id': int(pk), 'item_count': number_of_items,
            'description': new_list_item.description, 'created': new_list_item.created,
            'complete': new_list_item.complete}))
    return HttpResponse(None)



def convert_query_set_to_list(query_set_data):
    query_set_list = []
    for q in query_set_data:
        list_item = {}
        list_item = {k:v for(k,v) in q['fields'].items()}
        list_item['pk'] = q['pk']
        query_set_list.append(list_item)
    return query_set_list

@csrf_exempt
def all_to_do_lists(request):
    from django.core import serializers
    all_to_do_lists_data = serializers.serialize('json',
        TodoList.objects.all(),
        fields=('title','description', 'pk', 'created', 'due_date'))
    all_to_do_lists_data = convert_query_set_to_list(json.loads(all_to_do_lists_data))
    return HttpResponse(json.dumps(all_to_do_lists_data))

@csrf_exempt
def get_to_do_list(request, pk=None):
    from django.core import serializers
    to_do_list_obj = TodoList.objects.filter(pk=pk)[:1]
    to_do_list_obj_items = None
    try:
            to_do_list_obj_items = TodoListItem.objects.filter(to_do_list_id=pk)
    except:
        pass
    to_do_list_data = serializers.serialize('json',
        to_do_list_obj,
        fields=('title','description', 'pk', 'created', 'due_date',)
    )
    to_do_list_data = json.loads(to_do_list_data)
    #print(to_do_list_data.update({'list_items': '123'}))
    to_do_list_data[0]['list_items'] = None
    if to_do_list_obj_items is not None:
        to_do_list_items_data = serializers.serialize('json',
            to_do_list_obj_items,
            fields=('title','description', 'pk', 'created', 'complete')
        )
        to_do_list_data[0]['list_items'] = convert_query_set_to_list(json.loads(to_do_list_items_data))
    return HttpResponse(json.dumps(to_do_list_data))