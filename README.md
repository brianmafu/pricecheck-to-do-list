# PriceCheck-To-do-list

* 1. Install python 3
* 2. Install python-pip
* 3. Clone Project locally.
* 4. Change directory to project directory ( on the terminal)
* 5. run: pip install -r requirements.txt
* 6. Make sure you have an internet connection( as I am importing fonts from a third-party url)
* 7. run: python manage.py collectstatic 
* 8. Database is a sqlite3 database. I have also committed it as to eliminate the need to re-run migrations. Should you have an issue with the datase (db.sqlite3). 
 and Delete it and run: python manage.py migrate.
* 9. run: python manage.py runserver 127.0.0.1: 8001
* 10. Once project is running: open url: http://127.0.0.1:8001  on browser
* 11. Then you should be able to view the running home page
* NB. Project is a Python/Django Project using Boostrap 4
* Please let me know if you have any questions or any issues. (brianmafu@gmail.com/ +2765 911 0631)
